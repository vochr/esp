## Skript zur Verfügung gestellt von Gerald Kändler, per Email am 07.12.2021
## Daten aus DB: BWI3_HR_BW.mdb
require(RODBC)
con <- RODBC::odbcConnectAccess("H:/FVA-Projekte/P01770_SURGE_Pro/Daten/Urdaten/wp5/bw/bwi/BWI3_HR_BW.mdb")
ecken.3 <- RODBC::sqlFetch(con, "Eckenmerkmale_BWI3")
baeume.3 <- RODBC::sqlFetch(con, "BWI_3_Baeume_B")
RODBC::odbcClose(con)
#-------------------------------------------------------------------------------
#Auswertungen
#-------------------------------------------------------------------------------

#Fläche nach Bestandestypen
unique (ecken.3$BTyp)
#Fichtentypen: 100, 101, 107: Fichte >= 90 % 
#Misch-Typen mit Fi als führender Baumart: 110, 170, 117, 171

#------------------------------------------------------------------------------
#BWI 2012
#--------
#Berechnung der Fichten-Anteile an allen Traktecken
fi.ant.te.3 <- aggregate(cbind(fi.ant =baeume.3$StFl2*ifelse(baeume.3$BA%in%c(10:19),1,0),
                               snb.ant=baeume.3$StFl2*ifelse(baeume.3$BA%in%c(20:99),1,0),
                               lb.ant =baeume.3$StFl2*ifelse(baeume.3$BA%in%c(100:299),1,0),
                               bl.ibl =baeume.3$StFl2*ifelse(baeume.3$BA%in%c(998,999),1,0))/10000,
                         by=list(TNr=baeume.3$TNr,ENr=baeume.3$ENr),sum)

unique(apply(fi.ant.te.3[,3:6],MARGIN=1,sum))

head(fi.ant.te.3)
nrow(fi.ant.te.3)

#BTyp anfügen
fi.ant.te.3 <- merge(fi.ant.te.3,
                     subset(ecken.3,select = c(TNr,ENr,BTyp,HoeNN, NatHoe,WGNr_BW)),
                     by=c("TNr","ENr"))

fi.ant.te.3$fi.gr80 <- ifelse(fi.ant.te.3$BTyp%in%c(100,101,107) | 
                                (fi.ant.te.3$fi.ant>0.8 & fi.ant.te.3$BTyp%in%c(110,117,170,171)),
                              1, 0)
#Anzahl der Traktecken mit einem Fichtenanteil über 80 % 
sum(fi.ant.te.3$fi.gr80)
fi.ant.te.3.ge80 <- unique(fi.ant.te.3[fi.ant.te.3$fi.gr80==TRUE, c("TNr", "ENr")])
save(fi.ant.te.3.ge80, file="H:/FVA-Projekte/P01770_SURGE_Pro/Daten/Urdaten/wp5/bw/bwi/fi.ge80.rdata")

#ecken.3 um Attribut Fichtenanteil größer 80% ergänzen
ecken.3 <- merge(ecken.3,subset(fi.ant.te.3,select=c(TNr,ENr,fi.gr80)),by=c("TNr","ENr"),all.x=T)
head(ecken.3); nrow(ecken.3)


(fl.fi.best.2012 <- fl.stratum.fun(list(Wa=c(3,5),Begehbar=1,BTyp=c(100:171) ),ecken.3,trakte.3,A))
(fl.fi.rb.2012 <- fl.stratum.fun(list(Wa=c(3,5),Begehbar=1,BTyp=c(100,101,107) ),ecken.3,trakte.3,A))
(fl.fi.mb.2012 <- fl.stratum.fun(list(Wa=c(3,5),Begehbar=1,BTyp=c(110,117,170,171) ),ecken.3,trakte.3,A))
(fl.fi.gr80.2012 <- fl.stratum.fun(list(Wa=c(3,5),Begehbar=1,fi.gr80=1 ),ecken.3,trakte.3,A))

mean(fi.ant.te.3$fi.ant[fi.ant.te.3$BTyp%in%c(100,101,107)])
mean(fi.ant.te.3$fi.ant[fi.ant.te.3$fi.gr80==1])


#BWI 2002
fi.ant.te.2 <- aggregate(cbind(fi.ant =baeume.2$StFl2*ifelse(baeume.2$BA%in%c(10:19),1,0),
                               snb.ant=baeume.2$StFl2*ifelse(baeume.2$BA%in%c(20:99),1,0),
                               lb.ant =baeume.2$StFl2*ifelse(baeume.2$BA%in%c(100:299),1,0),
                               bl.ibl =baeume.2$StFl2*ifelse(baeume.2$BA%in%c(998,999),1,0))/10000,
                         by=list(TNr=baeume.2$TNr,ENr=baeume.2$ENr),sum)
head(fi.ant.te.2)
nrow(fi.ant.te.2)

#BTyp anfügen
fi.ant.te.2 <- merge(fi.ant.te.2,subset(ecken.2,select = c(TNr,ENr,BTyp,HoeNN, NatHoe,WGNr_BW)),by=c("TNr","ENr"))

fi.ant.te.2$fi.gr80 <- ifelse(fi.ant.te.2$BTyp%in%c(100,101,107) | (fi.ant.te.2$fi.ant>0.8 & fi.ant.te.2$BTyp%in%c(110,117,170,171)),1,0)

mean(fi.ant.te.2$fi.ant[fi.ant.te.2$BTyp%in%c(100,101,107)])
mean(fi.ant.te.2$fi.ant[fi.ant.te.2$fi.gr80==1])

#
sum(fi.ant.te.2$fi.gr80)

#ecken.2 um Attribut Fichtenanteil größer 80% ergänzen
ecken.2 <- merge(ecken.2,subset(fi.ant.te.2,select=c(TNr,ENr,fi.gr80)),by=c("TNr","ENr"),all.x=T)
head(ecken.2); nrow(ecken.2)

(fl.fi.best.2002 <- fl.stratum.fun(list(Wa=c(1:3),Begehbar=1,BTyp=c(100:171) ),ecken.2,trakte.2,A.12))
(fl.fi.rb.2002 <- fl.stratum.fun(list(Wa=c(1:3),Begehbar=1,BTyp=c(100,101,107) ),ecken.2,trakte.2,A.12))
(fl.fi.mb.2002 <- fl.stratum.fun(list(Wa=c(1:3),Begehbar=1,BTyp=c(110,117,170,171) ),ecken.2,trakte.2,A.12))
(fl.fi.gr80.2002 <- fl.stratum.fun(list(Wa=c(1:3),Begehbar=1,BTyp=c(100:171),fi.gr80=1 ),ecken.2,trakte.2,A.12))


#BWI 1987
fi.ant.te.1 <- aggregate(cbind(fi.ant =baeume.1$StFl1*ifelse(baeume.1$BA%in%c(10:19),1,0),
                               snb.ant=baeume.1$StFl1*ifelse(baeume.1$BA%in%c(20:99),1,0),
                               lb.ant =baeume.1$StFl1*ifelse(baeume.1$BA%in%c(100:299),1,0),
                               bl.ibl =baeume.1$StFl1*ifelse(baeume.1$BA%in%c(998,999),1,0))/10000,
                         by=list(TNr=baeume.1$TNr,ENr=baeume.1$ENr),sum)
head(fi.ant.te.1)
nrow(fi.ant.te.1)

#BTyp anfügen
fi.ant.te.1 <- merge(fi.ant.te.1,subset(ecken.1,select = c(TNr,ENr,BTyp,HoeNN, NatHoe,WGNr_BW)),by=c("TNr","ENr"))

fi.ant.te.1$fi.gr80 <- ifelse(fi.ant.te.1$BTyp%in%c(100,101,107) | (fi.ant.te.1$fi.ant>0.8 & fi.ant.te.1$BTyp%in%c(110,117,170,171)),1,0)

#
sum(fi.ant.te.1$fi.gr80)

mean(fi.ant.te.1$fi.ant[fi.ant.te.1$BTyp%in%c(100,101,107)])
mean(fi.ant.te.1$fi.ant[fi.ant.te.1$fi.gr80==1])


#ecken.1 um Attribut Fichtenanteil größer 80% ergänzen
ecken.1 <- merge(ecken.1,subset(fi.ant.te.1,select=c(TNr,ENr,fi.gr80)),by=c("TNr","ENr"),all.x=T)
head(ecken.1); nrow(ecken.1)
