#' build EFISCEN input data from NFI-data
#' here, we use the NFI raw data, i.e. the baum_wzp table.
#'
#' aim:
#' data set with columns
#' age class [20yr-classes]
#' area [ha]
#' stock volume [m3 ha^-1]
#' increment [m3 ha^-1 yr^-1]
#'
