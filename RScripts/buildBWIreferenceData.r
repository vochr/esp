#' this script downloads the BWI 2012 data and prepares those to be used for 
#' volume predictions using both BDAT and the new taper models
#' 
#' use of 32-bit R is required since, access to 32bit-MS-Access required

## note ####
if(!(R.version$arch=="i386")){
  stop("need 32-bit R to run")
}

## download and unzip bwi 2012 data ####
target <- tempdir(check = TRUE)
p <- "https://bwi.info/Download/de/BWI-Basisdaten/ACCESS2003/bwi20150320_alle_daten2012.zip"
temp <- tempfile()
download.file(url = p, destfile = temp)
unzip(temp, exdir = target)
unlink(temp) # destroy temporary file

## check file
f <- list.files(target, pattern = ".mdb")
# file.show(file.path(target, f))


## queries for area and stock ####
baanteil <- paste0("SELECT b3_bestock_baanteile.Tnr, b3_bestock_baanteile.Enr, ",
                   "b3_bestock_baanteile.Ba, b3_bestock_baanteile.Schicht, ", 
                   "b3_bestock_baanteile.Herkunft, b3_bestock_baanteile.AnteilBa ",
                   "FROM b3_bestock_baanteile;")

bestock <- paste0("SELECT b3_bestock.Tnr, b3_bestock.Enr, b3_bestock.BestockAb, ",
                  "b3_bestock.BestockAl, b3_bestock.BestockAl2012, ", 
                  "b3_bestock.BestockKal, b3_bestock.BestockTypFein, ",
                  "b3_bestock.BestockTypLN ",
                  "FROM b3_bestock;")

ecke <- paste0("SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_ecke.Bl ",
               "FROM b0_ecke;")

trakt <- paste0("SELECT b0_tab.Tnr, b0_tab.Netz, b0_tab.Netz64, b0_tab.Netz256, ",
                "b0_tab.HoeheNN ",
                "FROM b0_tab;")

baum <- paste0("SELECT b3_baeume.Tnr, b3_baeume.Enr, b3_baeume.Bnr, b3_baeume.Ba, ",
               "b3_baeume.Al_ba, b3_baeume.Bs, b3_baeume.Bhd, b3_baeume.Hoehe, ",
               "b3_baeume.D7, b3_baeume.D03, b3_baeume.G, b3_baeume.VolR, ",
               "b3_baeume.VolR_FAO, b3_baeume.N_ha, b3_baeume.N_ha_Hb, ",
               "b3_baeume.Stf, b3_baeume.StfM, b3_baeume.StfM_Hb ",
               "FROM b3_baeume ", 
               "WHERE b3_baeume.BHD >= 70;")

require(RODBC)
conn <- RODBC::odbcConnectAccess2007(file.path(target, f))
baanteil <- RODBC::sqlQuery(conn, query = baanteil)
bestock <- RODBC::sqlQuery(conn, query = bestock)
trakt <- RODBC::sqlQuery(conn, query = trakt)
ecke <- RODBC::sqlQuery(conn, query = ecke)
baum <- RODBC::sqlQuery(conn, query = baum)
RODBC::odbcClose(conn)

unlink(target, recursive = TRUE) # remove big source data (> 350MB!)


## download and unzip bwi 2002-2012 diff data ####
# target <- tempdir(check = TRUE)
# p <- "https://bwi.info/Download/de/BWI-Basisdaten/ACCESS2003/bwi20150320_alle_daten2002-2012.zip"
# temp <- tempfile()
# download.file(url = p, destfile = temp)
# unzip(temp, exdir = target)
# unlink(temp) # destroy temporary file
# 
# ## check file
# f <- list.files(target, pattern = ".mdb")
# # file.show(file.path(target, f))
# 
# ## increment query ####
# diff <- paste0("SELECT b23_baeume_m_s.Tnr, b23_baeume_m_s.Enr, b23_baeume_m_s.Bnr, ",
#                "b23_baeume_m_s.Ba, b23_baeume_m_s.Al_ba, b23_baeume_m_s.Bs, ",
#                "b23_baeume_m_s.Bhd, b23_baeume_m_s.VolR_FAO_Diff, b23_baeume_m_s.N_ha ",
#                "FROM b23_baeume_m_s;")
# 
# conn <- RODBC::odbcConnectAccess2007(file.path(target, f))
# diff <- RODBC::sqlQuery(conn, query = diff)
# RODBC::odbcClose(conn)

# unlink(target, recursive = TRUE) # remove big source data (> 76MB!)

## get WEHAM-dGZ100 values ####
pweham <- "H:/FVA-Projekte/P01770_SURGE_Pro/Daten/Urdaten/wp5/bw/weham"
if(!file.exists(file.path(pweham, "WEHAM_OUTPUTXX40.mdb"))){
  require(wehamr)
  file.show(file.path(pweham, "weham.ini"))
  weham(ini_file = file.path(pweham, "weham.ini"),
        control_name = "weham_steuer40.mdb",
        is_msaccess2007 = TRUE)
}

con <- RODBC::odbcConnectAccess(file.path(pweham, "WEHAM_OUTPUTXX40.mdb"))
dgz <- RODBC::sqlFetch(con, sqtable = "wehamo_bonitaet")
RODBC::odbcClose(con)
# indeed from all TNRxENR at WZP 2012, 228 have no Bonität information...
dgz2 <- subset(dgz, Ba==1)
dgz2$Ba <- 10 # remap species code from Ba==1 (species group) to weham_ba==1 (spruce)
dgz2$dgzc <- ((((dgz2$Bonitaet)+2) %/% 3) * 3) - 1 # classify according to 3 m3 vol-classes
sort(unique(dgz2$dgzc))
head(dgz2)
(any(is.na(dgz2$dgzc)))

## get BTyp 2012 ####
#' BTyp is BW-specific Traktecken-Attribut defined during NFI/BWI-field work
#' 100 = Fi
#' 101 = Fi + needle trees < 10% 
#' 107 = Fi + deciduous trees < 10% 
#' 110 = Fi + needle trees [10,50]%
#' 170 = Fi + deciduous trees [10,50]%
#' 117 = Fi + needle trees [10,50]% and decidous trees < 10%
#' 171 = Fi + deciduous trees [10,50]% and needle trees < 10%

btyp <- read.csv2("H:/FVA-Projekte/P01770_SURGE_Pro/Daten/Urdaten/wp5/bw/bwi/btyp_TE_BWI_2012.csv")
btyp <- subset(btyp, BTyp %in% c(100, 101, 107, 110, 170, 117, 171))

## build data frame ####
#' should look the following:
#' age [20yr classes] | dGz100 [m3/ha/a] | area [ha] | volume [FAO, m³/ha] | increment
#' conditioning on 
#' 1) area of Baden-Württemberg
#' 2) split data according to absolute site index (dGZ100) [4-6, 7-9, 10-12, 13-15, 17-19]
#' 3) only Norway spruce
#' 4) main stand (schicht==1); bare-forest-land in age class 0-20
#' 5) Norway spruce share >=80% (on each Traktecke)
#' 6) use FAO volume, i.e. from stump to tree top incl. bark (2m sections)

# only main stand (Schicht == 1), Norway spruce (Ba == 10) and Share >= 80% (AnteilBa >= 0.8)
head(baanteil)
## build table baanteil from table baum
head(baum)
baanteil2 <- aggregate(x = list(AnteilBa=baum$StfM * baum$N_ha / 10000), 
                       by = list(Tnr=baum$Tnr, Enr=baum$Enr, Ba=baum$Ba),
                       FUN = "sum")
baanteil2 <- baanteil2[order(baanteil2$Tnr, baanteil2$Enr, baanteil2$Ba),]
baanteil2 <- subset(baanteil2, Ba == 10 & AnteilBa >= 0.8)
baanteil2$TE <- baanteil2$Tnr*10+baanteil2$Enr
length(unique(baanteil2$TE)) == nrow(baanteil2) # exactly one entry per Traktecke
head(baanteil2) #> these Traktecken are to be used...

head(bestock)
head(trakt)

head(ecke)
ecke2 <- subset(ecke, Bl == 8) # only Baden-Württemberg
head(ecke2)

df <- merge(ecke2, baanteil2[, c("Tnr", "Enr", "AnteilBa")], 
            by.x = c("Tnr", "Enr"), by.y = c("Tnr", "Enr"))
head(df) #> these Traktecken in BW are to be used...
#> 2643 Traktecken (2740 Traktecken falls tbl baanteil von BWI3-DB genutzt wird)


## alternatively using Btyp-informations
# btyp, with only Traktecken in Baden-Württemberg
df <- merge(btyp, baanteil2[, c("Tnr", "Enr", "AnteilBa")], 
            by.x = c("TNr", "ENr"), by.y = c("Tnr", "Enr"))
head(df) #> these Traktecken in BW are to be used...
df <- subset(df, BTyp %in% c(100, 107, 101) | AnteilBa >= 0.8)
colnames(df) <- c("Tnr", "Enr", "BTyp", "AnteilBa")
head(df)
# 2602 Traktecken (2988 Traktecken falls tbl baanteil von BWI3-DB genutzt wird)

## Trakteckenauswahl nach Gerald Kändler
## vgl. Skript trackteckenauswahl_nach_GK.r im gleichen Verzeichnis
load("H:/FVA-Projekte/P01770_SURGE_Pro/Daten/Urdaten/wp5/bw/bwi/fi.ge80.rdata",
     envir = e <- new.env())
df <- e$fi.ant.te.3.ge80
colnames(df) <- c("Tnr", "Enr")
head(df)

head(baum)
#Bs=Bestandesschicht: 0-Plenter, 1-Hauptbestand, 2-Unterstand, 3-Oberstand
# 4-Verjüngung unter Schirm, 9-liegend 
baum2 <- subset(baum, Bs %in% c(0,1,3) & Ba == 10 & Bhd >= 70, 
               c("Tnr", "Enr", "Bnr", "Ba", "Bs", "Bhd", "Hoehe", "Al_ba", "VolR_FAO", "StfM", "N_ha"))
RFaktor <- 100.0545 # Hochrechnungsfaktor auf Gesamtland Baden-Württemberg taken from WEHAM
baum2$VolR_FAO_bw <- baum2$VolR_FAO * baum2$N_ha # m3/ha
baum2$StfM_bw <- baum2$StfM * baum2$N_ha * RFaktor / 10000 # ha
baum2$Al20 <- (baum2$Al_ba %/% 20) * 20
baum2$Al20 <- ifelse(baum2$Al20 > 140, 140, baum2$Al20)
baum2 <- merge(baum2, df, by = c("Tnr", "Enr"))
head(baum2)

## join dgz2 to baum ####
baum3 <- merge(baum2, dgz2[, c("Tnr", "Enr", "Ba", "Bonitaet", "dgzc")],
               by.x = c("Tnr", "Enr", "Ba"), by.y = c("Tnr", "Enr", "Ba"), 
               all.x = TRUE)
head(baum3)
unique(baum3$dgzc) # 
length(which(is.na(baum3$dgzc)))
View(baum3[is.na(baum3$dgzc),])

if(any(is.na(baum3$dgzc))){
  #' open question: how to treat NA values?
  #' this is just a bad indicative workaround the problem of NA in dgz100
  # baum3$dgzc <- ifelse(is.na(baum3$dgzc), 0, baum3$dgzc)
  ## another possibility
  require(dplyr)
  tmp <- baum3 %>% 
  group_by(Tnr, Enr) %>% 
  summarize(VolR_FAO = weighted.mean(VolR_FAO, ifelse(N_ha==0, 1, N_ha)),
            Bhd = weighted.mean(Bhd, ifelse(N_ha==0, 1, N_ha)),
            Hoehe = weighted.mean(Hoehe, ifelse(N_ha==0, 1, N_ha)),
            Al_ba = weighted.mean(Al_ba, ifelse(N_ha==0, 1, N_ha)),
            dgzc = mean(dgzc)) %>% 
            as.data.frame()
  str(tmp)
  boxplot(Al_ba ~ dgzc, data=tmp)
  boxplot(VolR_FAO ~ dgzc, data=tmp)
  boxplot(Bhd ~ dgzc, data=tmp)
  boxplot(Hoehe ~ dgzc, data=tmp)
  
  ## takes a while to repeatedly impute (if line 214 is commented)
  bias <- numeric(10)
  #bias <- numeric(1)
  for(i in seq(along=bias)){
    dgzc <- tmp$dgzc
    (nNA <- length(which(is.na(tmp$dgzc))))
    # add some artifical NAs for testing
    tmp[idx <- sample(1:nrow(tmp), size = nNA, replace = FALSE), "dgzc"] <- NA
    length(which(is.na(tmp$dgzc)))
    
    require(mice)
    imp <- mice(data = tmp, method = "pmm", m = 100, maxit = 50)
    tmp$dgzc2 <- tmp$dgzc
    length(which(is.na(tmp$dgzc2)))
    tmp$dgzc <- dgzc # reset to original values
    length(which(is.na(tmp$dgzc)))
    ## mean imputation ...
    tmp[is.na(tmp$dgzc2),]$dgzc2 <- apply(X = imp$imp$dgzc, MARGIN = 1, FUN = "mean")
    any(is.na(tmp$dgzc2))
    tmp$dgzc2 <- ((((tmp$dgzc2)+2) %/% 3) * 3) - 1 # classifying
    plot(dgzc2 ~ dgzc, data=tmp); abline(0,1)
    plot(dgzc2 ~ dgzc, data=tmp[idx,]); abline(0,1)
    summary(tmp[is.na(tmp$dgzc), "dgzc2"])
    summary(tmp[!is.na(tmp$dgzc), "dgzc2"])
    # View(tmp[idx,])
    bias[i] <- mean(tmp[idx,]$dgzc - tmp[idx,]$dgzc2, na.rm = TRUE)
  }
  mean(bias) # 0.3993818 (over 10  iterations, see line 213)
  sd(bias) # 0.09679459
  
  ## merge dgz estimate into baum3
  head(baum3)
  head(tmp)
  
  baum3 <- merge(baum3, tmp[, c("Tnr", "Enr", "dgzc2")], by = c("Tnr", "Enr"))
  baum3$dgzc <- ifelse(is.na(baum3$dgzc), baum3$dgzc2, baum3$dgzc)
  baum3$dgzc2 <- NULL
}

## some N_ha are zero
View(baum3[baum3$N_ha==0,])
#> 251 / 24217
#> 428 / 26226 # using traktecken from GK selection

## aggregate volume and area ####
df <- merge(df[, c("Tnr", "Enr")], 
            baum3[, c("Tnr", "Enr", "Bnr", "Ba", "Al_ba", "VolR_FAO_bw", "StfM_bw", "dgzc")],
            by.x = c("Tnr", "Enr"), by.y = c("Tnr", "Enr"))
df$Al20 <- (df$Al_ba %/% 20) * 20
df$Al20 <- ifelse(df$Al20 > 140, 140, df$Al20)
dfa1 <- aggregate(df[, c("StfM_bw")], 
                  by=list(Al20=df$Al20, dGZ100=df$dgzc), 
                  FUN = "sum")
colnames(dfa1) <- c("Al20", "dGZ100", "StfM_bw")
dfa2 <- aggregate(df[, c("VolR_FAO_bw")], 
                  by=list(Al20=df$Al20, dGZ100=df$dgzc), 
                  FUN = "mean")
colnames(dfa2) <- c("Al20", "dGZ100", "VolR_FAO_bw")
dfa <- merge(dfa1, dfa2)

## add increment from estimation ####
#' here we use estimates from the Baden-Württemberg analysis of the inventory data
#' this analysis is done by Gerald Kändler and stored in an xlsx sheet
require(RODBC)
pinc <- "H:/FVA-Projekte/P01770_SURGE_Pro/Daten/aufbereiteteDaten/BWI2EFISCEN"
con <- odbcConnectExcel2007(file.path(pinc, "Spruce_EFISCEN_BaWue_v1.xlsx"))
inc <- sqlFetch(con, sqtable = "mai_age_spruce_stands")
odbcClose(con)
inc <- inc[, c("AKL", grep("^lZ", colnames(inc), value = TRUE))]
colnames(inc) <- sub("lZ#", "", colnames(inc))
colnames(inc)[which(colnames(inc)=="dgz5#8")] <- "dgz5"
inc$dgz8 <- inc$dgz5
(inc$Al20 <- sub("^[0-9]{1,3}-", "", inc$AKL))
(inc$Al20 <- as.integer(ifelse(inc$Al20==">120", 140, inc$Al20)) - 20)
## add row for age 140+ (using data for 120+)
inc <- rbind(inc, data.frame(AKL = ">140", 
                             inc[which(inc$Al20==120), grep("^dgz", colnames(inc))],
                             Al20 = 140))
inc
inca <- tidyr::gather(inc[, -1], key = "dGZ100", value = "dgz", 
                      grep("^dgz", colnames(inc), value = TRUE))
inca$dGZ100 <- as.integer(sub("^dgz", "", inca$dGZ100))
str(inca)

res <- merge(dfa, inca, by = c("Al20", "dGZ100"), all.x = TRUE, all.y = TRUE)
res[is.na(res)] <- 0
(res <- res[order(res$dGZ100, res$Al20),])

# save file ####
rscript <- "H:/FVA-Projekte/P01770_SURGE_Pro/Programme/Eigenentwicklung/surgepro/RScripts/buildBWIreferenceData.r"
save(res, dfa, inca, rscript, 
     file = file.path("H:/FVA-Projekte/P01770_SURGE_Pro/Daten/aufbereiteteDaten", 
                      "BWI2EFISCEN", paste0("bw_spruce", ".rdata")))
